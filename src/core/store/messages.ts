import { makeObservable, observable, action } from 'mobx';

import { axiosRequest } from '../http';

type Message = {
    id: string,
    text: string,
    fromMe: boolean,
};

class MessagesService {
    messages: Message[] = [];
    phone: string = '';
    idInstance: string = '';
    apiTokenInstance: string = '';

    constructor() {
        makeObservable(this, {
            messages: observable,
            idInstance: observable,
            apiTokenInstance: observable,
            phone: observable,

            sendMessage: action,
            setMessages: action,
            setIdInstance: action,
            setApiTokenInstance: action,
            setPhone: action,
        });
    }

    setMessages = (idMessage: string, text: string, fromMe: boolean) => {
        this.messages = [...this.messages, {
            text,
            fromMe,
            id: idMessage,
        }];
    }

    setIdInstance = (idInstance: string) => {
        this.idInstance = idInstance;
    }

    setApiTokenInstance = (apiTokenInstance: string) => {
        this.apiTokenInstance = apiTokenInstance;
    }

    setPhone = (phone: string) => {
        this.phone = phone;
    }

    async sendMessage(text: string) {
        try {
            const data = await axiosRequest.post(`waInstance${this.idInstance}/SendMessage/${this.apiTokenInstance}`, {
                chatId: `${this.phone}@c.us`,
                message: text,
            });

            this.setMessages(data.data.idMessage, text, true);

            return true;
        } catch (e) {
            console.error(e);
        }
    };

    async receiveNotifications() {
        const { data } = await axiosRequest.get(`waInstance${this.idInstance}/ReceiveNotification/${this.apiTokenInstance}`);

        if (data) {
            await axiosRequest.delete(`waInstance${this.idInstance}/DeleteNotification/${this.apiTokenInstance}/${data.receiptId}`);
            const { body } = data;
            if (body.typeWebhook === 'incomingMessageReceived') {
                this.setMessages(body.idMessage, body.messageData.textMessageData.textMessage, false);
            }
        }

        this.receiveNotifications();
    }
}

export const messagesService = new MessagesService();
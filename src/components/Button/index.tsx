import React from 'react';

import './style.scss';

type Props = {
    text: string,
    onClick: () => void,
}

const Button = ({ text, onClick }: Props) => {
    return (
        <button onClick={onClick} className="Button">
            <span className="Button__text">{text}</span>
        </button>
    )
};

export { Button };
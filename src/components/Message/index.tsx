import React from 'react';
import classNames from 'classnames';

import './style.scss';

type Props = {
    text: string,
    fromMe: boolean,
}

const Message = ({ text, fromMe }: Props) => {
    return (
        <div className={classNames('Message', { 'Message_fromMe': fromMe })}>
            <div className={classNames('Message__content', { 'Message__content_fromMe': fromMe })}>
                {text}
            </div>
            <span className={classNames('Message__tail', { 'Message__tail_left': !fromMe })} />
        </div>
    )
};

export { Message };
import React from 'react';

import './style.scss';

type Props = {
    label?: string,
    value: string,
    onChangeValue: (value: string) => void,
    onKeyDown?: (event: React.KeyboardEvent) => void,
}

const Input = ({ label, value, onChangeValue, onKeyDown }: Props) => {
    const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        onChangeValue(event.target.value);
    }
    return (
        <div className="Input">
            <span className="Input__label">{label}</span>
            <input className="Input__input" value={value} onInput={onChange} onKeyDown={onKeyDown} />
        </div>
    )
};

export { Input };
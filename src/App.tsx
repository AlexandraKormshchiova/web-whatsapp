import React from 'react';
import { observer } from 'mobx-react';

import './App.scss';

import { LoginScreen } from './screens/LoginScreen';
import { ChatScreen } from './screens/ChatScreen';
import { messagesService } from './core/store/messages';

const App = () => {
    const { phone, apiTokenInstance, idInstance} = messagesService;

    return (
        <div className='App'>
            {apiTokenInstance && idInstance && phone ? (
                <ChatScreen />
            ) : (
                <LoginScreen />
            )}
        </div>
    )
}

export default observer(App);

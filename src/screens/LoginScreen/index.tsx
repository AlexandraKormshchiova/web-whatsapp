import React, { useEffect, useState } from 'react';
import classNames from 'classnames';

import { Input } from '../../components/Input';
import { Button } from '../../components/Button';

import { messagesService } from '../../core/store/messages';

import './style.scss';

const LoginScreen = () => {
    const [idInstance, setIdInstance] = useState('');
    const [apiTokenInstance, setApiTokenInstance] = useState('');
    const [phone, setPhone] = useState('');

    const [phoneFieldHidden, setPhoneFieldHidden] = useState(true);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        setIsError(false);
    }, [idInstance, apiTokenInstance, phone])

    const onContinue = () => {
        if (!(idInstance && apiTokenInstance) || (!phoneFieldHidden && !phone)) {
            setIsError(true);
        }

        if (phoneFieldHidden) {
            setPhoneFieldHidden(false);
        }

        if (phone) {
            messagesService.setApiTokenInstance(apiTokenInstance);
            messagesService.setIdInstance(idInstance);
            messagesService.setPhone(phone);

            messagesService.sendMessage('Hello! Now I use my own WhatsApp 😜');

            messagesService.receiveNotifications();
        }
    }

    return (
       <div className='LoginScreen'>
           <div className='LoginScreen__field'>
               <Input label='IdInstance' value={idInstance} onChangeValue={setIdInstance} />
           </div>
           <div className='LoginScreen__field'>
               <Input label='ApiTokenInstance' value={apiTokenInstance} onChangeValue={setApiTokenInstance} />
           </div>
           <div className={classNames(
               'LoginScreen__field',
               'LoginScreen__field-phone',
               { 'LoginScreen__field-phone_opened': !phoneFieldHidden })
           }>
               <Input label='Phone' value={phone} onChangeValue={setPhone} />
           </div>
           <Button text='Продолжить' onClick={onContinue} />
           <span className='LoginScreen__error-text'>
               {isError ? 'Заполните все поля' : ''}
           </span>
       </div>
    )
};

export { LoginScreen };
import React, { useState } from 'react';
import { observer } from 'mobx-react';

import { messagesService } from '../../core/store/messages';
import { Input } from '../../components/Input';
import { Message } from '../../components/Message';

import './style.scss';

const Chat = () => {
    const [message, setMessage] = useState('');
    const messages = messagesService.messages;

    const sendMessage = () => {
        if (!message) return;

        messagesService.sendMessage(message).then(() => {
            setMessage('');
        });
    };

    const handleKeyDown = (event: React.KeyboardEvent) => {
        if (event.key === 'Enter') {
            sendMessage();
        }
    }

    return (
        <div className='ChatScreen'>
            <div className='ChatScreen__header'>
                <div className='ChatScreen__header-pfp-icon' />
                <span className='ChatScreen__header-phone-number'>+{messagesService.phone}</span>
            </div>

            <div className='ChatScreen__field'>
                <div className='ChatScreen__input'>
                    <Input value={message} onChangeValue={setMessage} onKeyDown={handleKeyDown} />
                </div>
                <button onClick={sendMessage} className='ChatScreen__send-button'>
                    <img className='ChatScreen__send-image' src={require('../../assets/send-icon.png')} alt='Send' />
                </button>
            </div>

            <div className='ChatScreen__messages-container'>
                {messages.map((message) => (
                    <Message text={message.text} fromMe={message.fromMe} key={message.id} />
                ))}
            </div>
        </div>
    )
};

const ChatScreen = observer(Chat);

export { ChatScreen };
